package com.questmanager.common.DTO;

import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

public class AdventurerModelDTO {

    private Long id;
    private Integer pv;
    private String name;
    private String description;
    private Integer level;
    private Integer experiencePoints;
    private List<AttributeReferenceDTO> listAttributes = new ArrayList<>();


    public AdventurerModelDTO(Long id, Integer pv, String name, String description, Integer level, Integer experiencePoints, List<AttributeReferenceDTO> listAttributes) {
        this.id = id;
        this.pv = pv;
        this.name = name;
        this.description = description;
        this.level = level;
        this.experiencePoints = experiencePoints;
        this.listAttributes = listAttributes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPv() {
        return pv;
    }

    public void setPv(Integer pv) {
        this.pv = pv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(Integer experiencePoints) {
        this.experiencePoints = experiencePoints;
    }

    public List<AttributeReferenceDTO> getListAttributes() {
        return listAttributes;
    }

    public void setListAttributes(List<AttributeReferenceDTO> listAttributes) {
        this.listAttributes = listAttributes;
    }
}

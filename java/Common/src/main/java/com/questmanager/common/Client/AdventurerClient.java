package com.questmanager.common.Client;


import com.questmanager.common.DTO.User.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.questmanager.common.DTO.*;

@FeignClient(name="Adventurer",url= "http://localhost:8000"/*,configuration = {FeignInterceptor.class}*/)
public interface AdventurerClient {


    @RequestMapping(method= RequestMethod.GET,value="/adventurers/{id}")
    AdventurerModelDTO getAdventurer(@PathVariable("id") Long id);


    @RequestMapping(method= RequestMethod.GET,value="/attributesModel/{nameAttribute}")
    UserDTO getAttributeModelByName(@PathVariable("nameAttribute") String nameAttribute);

    @RequestMapping(method= RequestMethod.POST,value="/attributesModel/add")
    void addAttributeModel(@RequestBody UserDTO attributeModelDTO);

    @RequestMapping(method= RequestMethod.GET,value="/attributesReference/{id}")
    AttributeReferenceDTO getAttributeReference(@PathVariable("id") String id);

    @RequestMapping(method= RequestMethod.POST,value="/attributesReference/add")
    void addAttributeReference(@RequestBody AttributeReferenceDTO attributeReferenceDTO);
}

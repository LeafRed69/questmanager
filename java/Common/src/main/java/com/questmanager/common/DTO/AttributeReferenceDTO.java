package com.questmanager.common.DTO;


import com.questmanager.common.DTO.User.UserDTO;

public class AttributeReferenceDTO {

    private Long id;
    private Integer levelAttribute;
    private Long actualExperiencePoint;
    private AdventurerModelDTO adventurerModel;
    private UserDTO attributeModel;


    public AttributeReferenceDTO(Long id, Integer levelAttribute, Long actualExperiencePoint, AdventurerModelDTO adventurerModel, UserDTO attributeModel) {
        this.id = id;
        this.levelAttribute = levelAttribute;
        this.actualExperiencePoint = actualExperiencePoint;
        this.adventurerModel = adventurerModel;
        this.attributeModel = attributeModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLevelAttribute() {
        return levelAttribute;
    }

    public void setLevelAttribute(Integer levelAttribute) {
        this.levelAttribute = levelAttribute;
    }

    public Long getActualExperiencePoint() {
        return actualExperiencePoint;
    }

    public void setActualExperiencePoint(Long actualExperiencePoint) {
        this.actualExperiencePoint = actualExperiencePoint;
    }

    public AdventurerModelDTO getAdventurerModel() {
        return adventurerModel;
    }

    public void setAdventurerModel(AdventurerModelDTO adventurerModel) {
        this.adventurerModel = adventurerModel;
    }

    public UserDTO getAttributeModel() {
        return attributeModel;
    }

    public void setAttributeModel(UserDTO attributeModel) {
        this.attributeModel = attributeModel;
    }
}

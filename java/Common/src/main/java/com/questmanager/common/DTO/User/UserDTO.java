package com.questmanager.common.DTO.User;

import java.util.Set;

public class UserDTO {

    private Long id;
    private String login;
    private String lastName;
    private String surName;
    private String email;
    private Long idPlayer;
    private Set<Long> idfriends;


    public UserDTO(Long id, String login, String lastName, String surName, String email, Long idPlayer, Set<Long> idfriends) {
        this.id = id;
        this.login = login;
        this.lastName = lastName;
        this.surName = surName;
        this.email = email;
        this.idPlayer = idPlayer;
        this.idfriends = idfriends;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdPlayer() {
        return idPlayer;
    }

    public void setIdPlayer(Long idPlayer) {
        this.idPlayer = idPlayer;
    }

    public Set<Long> getIdfriends() {
        return idfriends;
    }

    public void setIdfriends(Set<Long> idfriends) {
        this.idfriends = idfriends;
    }
}

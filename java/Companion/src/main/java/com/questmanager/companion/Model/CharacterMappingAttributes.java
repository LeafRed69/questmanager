package com.questmanager.companion.Model;

import com.questmanager.companion.Model.Attribute.CharacterAttribute;
import com.questmanager.companion.Model.Attribute.SpeedAttribute;
import com.questmanager.companion.Model.Attribute.StrengthAttribute;

import javax.persistence.*;
import java.util.Map;

@Entity
public class CharacterMappingAttributes {

    // 1 mapping par character qui va lister tout les Attributs

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique=true, nullable = false)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "strengthAttribute_id", referencedColumnName = "id")
    private StrengthAttribute strengthAttribute;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "speedAttribute_id", referencedColumnName = "id")
    private SpeedAttribute speedAttribute;

    public CharacterMappingAttributes() {

    }
    public CharacterMappingAttributes(Map<String,CharacterAttribute> dictCharacterAttribute){
        this.strengthAttribute = new StrengthAttribute(dictCharacterAttribute.get("Strength"));
        this.speedAttribute = new SpeedAttribute(dictCharacterAttribute.get("Speed"));
    }
    public void initAttributes(Map<String,CharacterAttribute> dictCharacterAttribute){
        this.strengthAttribute = new StrengthAttribute(dictCharacterAttribute.get("Strength"));
        this.speedAttribute = new SpeedAttribute(dictCharacterAttribute.get("Speed"));

    }



}

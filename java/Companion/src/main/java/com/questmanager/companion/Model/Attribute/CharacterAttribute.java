package com.questmanager.companion.Model.Attribute;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class CharacterAttribute implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "nameId",unique=true, nullable = false)
    private String nameId;
    private String name;
    private String mathMethodStringCalcul;
    private String description;


    public CharacterAttribute(String id, String name, String mathMethod, String description) {
        /*this.name="Force";
        this.mathMethodStringCalcul="level * 2";
        this.description="De la force brute pour frapper fort";*/
        this.nameId=id;
        this.name=name;
        this.mathMethodStringCalcul=mathMethod;
        this.description=description;
    }

    public CharacterAttribute() {

    }


    public String getMathMethodStringCalcul() {
        return mathMethodStringCalcul;
    }



}

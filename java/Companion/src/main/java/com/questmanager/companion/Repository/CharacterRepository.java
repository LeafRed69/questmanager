package com.questmanager.companion.Repository;

import com.questmanager.companion.Model.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CharacterRepository extends CrudRepository<Character, Long> {

    Optional<Character> findCharacterById(Long id);
}

package com.questmanager.companion.Service;

import com.questmanager.companion.Model.Attribute.CharacterAttribute;


import com.questmanager.companion.Model.CharacterMappingAttributes;
import com.questmanager.companion.Repository.CharacterAttributeRepository;
import com.questmanager.companion.Repository.CharacterMappingAttributesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CharacterAttributeService {


    @Autowired
    private CharacterMappingAttributesRepository characterMappingAttributesRepository;

    @Autowired
    private CharacterAttributeRepository characterAttributeRepository;



    public Optional<CharacterAttribute> getCharacterAttribute(String id){
        return characterAttributeRepository.findCharacterAttributeByNameId(id);
    }
    public void saveCharacterAttribute(CharacterAttribute characterAttribute){
        characterAttributeRepository.save(characterAttribute);
    }

    public void saveCharacterMappingAttributes(CharacterMappingAttributes characterMappingAttributes){
        characterMappingAttributesRepository.save(characterMappingAttributes);
    }
/*
    public Optional<AttributeModel> findAttributeModelByNameAttribute(String nameAttribute){
        return attributeModelRepository.findAttributeModelByNameAttribute(nameAttribute);
    }

    public void addAttributeModel(AttributeModel myAttribute) {
        attributeModelRepository.save(myAttribute);
    }

    public void addAttributeReference(AttributeReference myPersonnalAttribute) {
        attributeReferenceRepository.save(myPersonnalAttribute);
    }

*/

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        /*CharacterAttribute forceAttribute = new CharacterAttribute("Force", "level * 10", "La force brute d'une personne");

        AttributeModel fouilleAttribute = new AttributeModel("Fouille", "level * 10");
        this.addAttributeModel(fouilleAttribute);

        adventurerCreateService.init();*/

        /*
        AttributeReference myPersonnalAttribute = new AttributeReference(10, 0L, VigeurAttribute,);
        this.addAttributeReference(myPersonnalAttribute);
        AttributeReference myPersonnalAttribute2 = new AttributeReference(VigeurAttribute);
        this.addAttributeReference(myPersonnalAttribute2);
        */
    }
}

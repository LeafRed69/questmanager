package com.questmanager.companion.Service;

import com.questmanager.companion.Model.Attribute.CharacterAttribute;
import com.questmanager.companion.Model.Character;
import com.questmanager.companion.Model.CharacterMappingAttributes;
import com.questmanager.companion.Repository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class CharacterCreateService {

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private CharacterAttributeService characterAttributeService;



    public Optional<Character> getCharacter(Long id) {
        return characterRepository.findCharacterById(id);
    }


    private void addCharacter(Character myCharacter) {
        characterRepository.save(myCharacter);
    }









    @EventListener(ApplicationReadyEvent.class)
    public void init(){

        CharacterAttribute strengthAttribute = new CharacterAttribute("Strength", "Force", "level * 2", "De la force brute pour frapper fort" );
        CharacterAttribute speedAttribute = new CharacterAttribute("Speed", "SUPER VIIIITESSE", "level * 3", "default description" );

        characterAttributeService.saveCharacterAttribute(strengthAttribute);
        characterAttributeService.saveCharacterAttribute(speedAttribute);



        Map<String,CharacterAttribute> dictCharacterAttribute = new HashMap<>();
        dictCharacterAttribute.put("Strength",characterAttributeService.getCharacterAttribute("Strength").get());
        dictCharacterAttribute.put("Speed",characterAttributeService.getCharacterAttribute("Speed").get());

        CharacterMappingAttributes characterMappingAttributes = new CharacterMappingAttributes(dictCharacterAttribute);
        characterAttributeService.saveCharacterMappingAttributes(characterMappingAttributes);

        Character character0 = new Character("Adams", "An old citizens of yelm", characterMappingAttributes);

        this.addCharacter(character0);

/*        AttributeReference myAttributeVigueur = new AttributeReference(10, 0L,
                attributeModelRestController.getAttributeModelByName("Vigueur").get(), adventurer0);

        attributeReferenceRestController.addAttributeReference(myAttributeVigueur);
        adventurer0.addNewAttribute(myAttributeVigueur);


        AttributeReference myAttributeForce = new AttributeReference(10, 0L,
                attributeModelRestController.getAttributeModelByName("Force").get(), adventurer0);

        attributeReferenceRestController.addAttributeReference(myAttributeForce);
        adventurer0.addNewAttribute(myAttributeForce);


        AttributeReference myAttributeFouille = new AttributeReference(10, 0L,
                attributeModelRestController.getAttributeModelByName("Fouille").get(), adventurer0);

        attributeReferenceRestController.addAttributeReference(myAttributeFouille);
        adventurer0.addNewAttribute(myAttributeFouille);*/



    }




}

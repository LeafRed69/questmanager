package com.questmanager.companion.Repository;


import com.questmanager.companion.Model.CharacterMappingAttributes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterMappingAttributesRepository extends CrudRepository<CharacterMappingAttributes, Long> {


}

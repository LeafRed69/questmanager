package com.questmanager.companion.Model.Attribute;

import net.objecthunter.exp4j.ExpressionBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class SpeedAttribute implements Serializable {

    private static final long serialVersionUID = 13L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique=true, nullable = false)
    private Long id;
    private Integer level;
    private Long actualExperiencePoint;

    @ManyToOne
    private CharacterAttribute characterAttribute;


    public SpeedAttribute(CharacterAttribute attributeName) {
        this.level=1;
        this.actualExperiencePoint=0L;
        this.characterAttribute = attributeName;

    }

    public SpeedAttribute() {

    }


    public void levelUp(){
        this.level++;
    }

    public void gainExperience(Long expPointGained){
        if (this.actualExperiencePoint+expPointGained>=this.getExperienceReachPoint()){

            this.levelUp();
            this.gainExperience(this.actualExperiencePoint+expPointGained-this.getExperienceReachPoint());

        }
        else this.setExperiencePoint(this.actualExperiencePoint+expPointGained);
    }




    public Long getExperienceReachPoint(){

        return Double.valueOf(
                new ExpressionBuilder(characterAttribute.getMathMethodStringCalcul())
                        .variable("level")
                        .build()
                        .setVariable("level", getLevelAttribute())
                        .evaluate()
        ).longValue();

    }


    public Integer getLevelAttribute() {
        return level;
    }
    private void setLevelAttribute(Integer levelAttribute) {
        this.level = levelAttribute;
    }

    public Long getExperiencePoint() {
        return actualExperiencePoint;
    }
    private void setExperiencePoint(Long experiencePoint) {
        this.actualExperiencePoint = experiencePoint;
    }


}

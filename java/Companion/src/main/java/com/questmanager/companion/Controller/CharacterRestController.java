package com.questmanager.companion.Controller;

import com.questmanager.companion.Model.Character;
import com.questmanager.companion.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
public class CharacterRestController {

    @Autowired
    private CharacterCreateService characterCreateService;


    @RequestMapping(method= RequestMethod.GET,value="/characters/{id}")
    public Optional<Character> getAdventurer(@PathVariable Long id) {
        Optional<Character> oCharacter;
        oCharacter= characterCreateService.getCharacter(id);
        return oCharacter;
    }

    /*@RequestMapping(method= RequestMethod.POST,value="/adventurers/regenerate")
    public Optional<AdventurerModel> getAdventurer(@PathVariable Long id) {
        Optional<AdventurerModel> rcompanion;
        rcompanion= adventurerCreateService.getAdventurer(id);
        return rcompanion;
    }*/







}

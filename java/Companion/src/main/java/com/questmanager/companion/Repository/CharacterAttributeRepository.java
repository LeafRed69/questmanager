package com.questmanager.companion.Repository;

import com.questmanager.companion.Model.Attribute.CharacterAttribute;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CharacterAttributeRepository extends CrudRepository<CharacterAttribute, String> {

    Optional<CharacterAttribute> findCharacterAttributeByNameId(String nameId);

}


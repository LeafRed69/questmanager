package com.questmanager.companion.Model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Character implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique=true, nullable = false)
    private Long id;
    private String name;
    private Date generateDate;
    private String description;


    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "characterMappingAttributes_id", referencedColumnName = "id")
    private CharacterMappingAttributes characterMappingAttributes;


    public Character(String name, String description, CharacterMappingAttributes characterMappingAttributes) {
        this.name = name;
        this.description = description;
        this.characterMappingAttributes = characterMappingAttributes;
    }
    public Character(String name, String description) {
        this.name = name;
        this.description = description;
    }
    public Character() {

    }
}

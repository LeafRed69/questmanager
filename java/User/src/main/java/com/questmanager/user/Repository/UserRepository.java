package com.questmanager.user.Repository;


import org.apache.catalina.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
	List<User> findByLoginAndPwd(String login, String pwd);
	User findFirstByLoginAndPwd(String login,String pwd);
	User findFirstByLogin(String login);
}

package com.questmanager.user.Service;



import com.questmanager.user.Repository.UserRepository;
import com.questmanager.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.util.*;


@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;


	public List<User> getAllUsers() {
		List<User> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}


	public Optional<User> getUser(String id) {
		return userRepository.findById(Long.valueOf(id));
	}

	public Optional<User> getUser(Long id) {
		return userRepository.findById(id);
	}

	public User getByUsername(String username){
		User user = userRepository.findFirstByLogin(username);
		if(user == null){
			return new User();
		}
		return user;}

	public UserDto addUser(User user) {
		// needed to avoid detached entity passed to persist error
		System.out.println(user.getLogin());
		Preference p = user.getPreference();
		if(p == null || p.getId() == 0){
			p = preferenceRepository.findById(1L).get();
			user.setPreference(p);
		}
		User u =  userRepository.save(user);
		fridgesClient.createFridge(new FridgeDto(0L,u.getLogin()+"'s fridge",u.getId(),new ArrayList<>()));
		return u.toDto();
	}

	public void updateUser(User user)
	{
		Preference p = user.getPreference();
		if(p == null || p.getId() == 0){
			p = preferenceRepository.findById(1L).get();
			user.setPreference(p);
		}
		user.setPwd(userRepository.findById(user.getId()).get().getPwd());
		userRepository.save(user);
	}

	public void deleteUser(String id) {
		userRepository.deleteById(Long.valueOf(id));
	}

	public UserDto getFirstUserByLoginAndPwd(UserDtoWithPassword userDTO) throws FunctionalException {
		User user = userRepository.findFirstByLoginAndPwd(userDTO.getLogin(),userDTO.getPwd());
		if(user==null){
			throw new FunctionalException("Ca ne marche pas.");
		}
		UserDto userDto  =user.toDto();
		return userDto;
	}

	public List<User> getUserByLoginPwd(String login, String pwd) {
		List<User> ulist=null;
		ulist=userRepository.findByLoginAndPwd(login,pwd);
		return ulist;
	}



	@EventListener(ApplicationReadyEvent.class)
	public void init() throws IOException, CsvException {
		boolean initBDD = false;
		if (initBDD) {
			File directoryPath = new File("/src/main/resources");
			//List of all files and directories
			String contents[] = directoryPath.list();
			System.out.println("List of files and directories in the specified directory:");
			for (int i = 0; i < contents.length; i++) {
				System.out.println(contents[i]);
			}

			System.out.println("\n\n\n\n\n\n =========================================================\n");


			List<User> beans = new CsvToBeanBuilder(new FileReader(ResourceUtils.getFile("/src/main/resources/user.csv")))
					.withType(User.class)
					.withSeparator(';')
					.build()
					.parse();
			CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
			CSVReader readerAllergies = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/userAllergies.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rAllergies = readerAllergies.readAll();
			CSVReader readerFriends = new CSVReaderBuilder(
					new FileReader(ResourceUtils.getFile("/src/main/resources/userFriends.csv")))
					.withCSVParser(csvParser)   // custom CSV parser
					.withSkipLines(1)           // skip the first line, header info
					.build();
			List<String[]> rFriends = readerFriends.readAll();

			Set<Long> temp;
			for (User bean : beans) {
				if (this.getByUsername(bean.getLogin()).getId() == null) {
					for (String[] str : rAllergies) {
						if (Long.parseLong(str[0]) == bean.getId()) {
							temp = bean.getAllergies();
							temp.add(Long.parseLong(str[1]));
							bean.setAllergies(temp);
						}
					}
					for (String[] str : rFriends) {
						if (Long.parseLong(str[0]) == bean.getId()) {
							temp = bean.getIdfriends();
							temp.add(Long.parseLong(str[1]));
							bean.setIdfriends(temp);
						}
					}
					this.updateUser(bean);
				}

			}

/*
		UserModel user=new UserModel("mat","gg");
		Set<Long> test = user.getAllergies();
		test.add(3L);
		test.add(4L);
		user.setAllergies(test);
		UserDto user2 = this.addUser(user);
		try(		Writer writer = Files.newBufferedWriter(ResourceUtils.getFile("classpath:test.csv").toPath());

				)
		{
			StatefulBeanToCsv<UserModel> beanToCsv = new StatefulBeanToCsvBuilder(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
					.withSeparator(';')
					.withMappingStrategy()
					.build();
			System.out.println("coucou "+ResourceUtils.getFile("classpath:test.csv").toPath());
			List<UserModel> myUsers = this.getAllUsers();
			System.out.println(myUsers.size());
			beanToCsv.write(myUsers);*/


		}
	}


}

package com.questmanager.user.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class User implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String login;
	private String pwd;
	private String lastName;
	private String surName;
	private String email;

	private Long idPlayer;

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Long> idfriends = new HashSet<>();



	public User() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}
/*
	public UserModel(UserDto userDto){
		this.id = userDto.getId();
		this.login = userDto.getLogin();
		this.pwd = "";
		if (userDto.getPreferenceId() == null) {
			this.preference = new Preference();
		}
		else{
			this.preference = new Preference(userDto.getPreferenceId(),userDto.getPreferenceName());
		}
		this.lastName=userDto.getLastName();
		this.surName=userDto.getSurName();
		this.email=userDto.getEmail();
	}

	public UserModel(UserDtoWithPassword userDto){
		this.id = userDto.getId();
		this.login = userDto.getLogin();
		this.pwd = userDto.getPwd();
		this.lastName=userDto.getLastName();
		this.surName=userDto.getSurName();
		this.email=userDto.getEmail();
		if (userDto.getPreferenceId() == null) {
			this.preference = new Preference();
		}
		else{
			this.preference = new Preference(userDto.getPreferenceId(),userDto.getPreferenceName());
		}
	}

	public UserModel(String login, String pwd) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}
*/
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}



	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Long> getIdfriends() {
		return idfriends;
	}

	public void setIdfriends(Set<Long> idfriends) {
		this.idfriends = idfriends;
	}

	public UserDto toDto(){
		return new UserDto(this.id, this.login, this.lastName, this.surName, this.email, this.player.toDto(),this.idfriends);
	}

	public UserDtoWithPassword toDtoWithPassword(){
		UserDto temp = new UserDto(this.id, this.login, this.lastName, this.surName, this.email, this.player.toDto(),this.idfriends);
		return new UserDtoWithPassword(temp, this.pwd);
	}

}
